# The packaging information for consul-replicate in XiVO

This repository contains the packaging information for [consul-replicate](https://github.com/hashicorp/consul-replicate).

To get a new version of consul-replicate in the XiVO repository, set the desired
version in the `VERSION` file, update the changelog version.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
